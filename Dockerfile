FROM php:8.1.3-apache
WORKDIR /app
COPY . .
RUN docker-php-ext-install mysqli
RUN apt-get update
RUN apt-get install vim -y