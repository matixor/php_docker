<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Video On Demand</title>

    <link rel="stylesheet" href="./css/style.css" />
  </head>
  <body>


  <?php
    $servername = "mysql";
    $username = "root";
    $password = "password";
    $dbname = "dane3";
    
    $conn = new mysqli($servername, $username, $password, $dbname);
    
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }
  ?>
    <header>
      <div class="left">
        <h1>Internetwowa wypożyczalnia filmów</h1>
      </div>
      <div class="right">
        <table>
          <tr>
            <td>Kryminał</td>
            <td>Horror</td>
            <td>Przygodowy</td>
          </tr>
          <tr>
            <td>20</td>
            <td>30</td>
            <td>20</td>
          </tr>
        </table>
      </div>
    </header>
    <main>
      <section id="polecamy">
        <h3>Polecamy</h3>
        <div class="movie">
          <!-- <figure class="box box1">
            <h4>18.Allen4</h4>
            <img src="./img/alien.jpg" alt="" />
            <figcaption>
              Zaloga statku Nostromo odbiera tajemniczy sygnal i laduje na
              planetoidzie...
            </figcaption>
          </figure>
          <figure class="box box2">
            <h4>22.Koralowa wyspa</h4>
            <img src="./img/koralowa_wyspa.jpg" alt="" height="150px" width="226px"/>
            <figcaption>
              Hamprey Bogart w jednej ze swoich wielkich rol.
            </figcaption>
          </figure>
          <figure class="box box3">
            <h4>23.krokodyl</h4>
            <img src="./img/krokodyl.jpg" alt="" />
            <figcaption>Gigantyczny krokodyl atakuje penwe miasto</figcaption>
          </figure>
          <figure class="box box4">
            <h4>25.SteamPunk</h4>
            <img src="./img/steampunk.jpg" alt="" />
            <figcaption>Wizja postapokaliptycznej planety</figcaption>
          </figure> -->

          <?php
            $sql = 'SELECT id,nazwa,opis,zdjecie FROM produkty Where id IN(18,22,23,25)';
            $result = $conn->query($sql);
            
            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) {
                echo "
                <figure>
                  <h4>".$row['id'].".".$row['nazwa']."</h4>
                  <img src='./img/".$row['zdjecie']."' height='150px' width='226px'/>
                  <figcaption>"
                    .$row['opis']."
                  </figcaption>
                </figure>";
              }
            } else {
              echo "0 results";
            }
          ?>
        </div>
      </section>
      <section id="fantastyczne">
        <h3>Filmy fantastyczne</h3>
        <div class="movie">
        <?php
            $sql1 = "SELECT id,nazwa,opis,zdjecie FROM produkty Where Rodzaje_id = 12 LIMIT 4;";
            $result1 = $conn->query($sql1);
            
            if ($result1->num_rows > 0) {
              while($row1 = $result1->fetch_assoc()) {
                echo "
                <figure>
                  <h4>".$row1['id'].".".$row1['nazwa']."</h4>
                  <img src='./img/".$row1['zdjecie']."' height='150px' width='226px'/>
                  <figcaption>"
                    .$row1['opis']."
                  </figcaption>
                </figure>";
              }
            } else {
              echo "0 results";
            }
          ?>
          </div>
      </section>
    </main>
    <footer>
      <form action="" method="post">
        <label for="deleteMovie">Usuń film nr.:</label>
        <input type="number" name="delete" id="deleteMovie" />
        <input type="submit" value="Usuń film" name="sub" />
        <p>Stonę wykonał: XYZ</p>

      </form>
        <?php
            $sumbit = $_POST['sub'];

            $movie_id = $_POST['delete'];

            if(isset($sumbit)){
            $sql2 = "DELETE FROM produkty WHERE id = $movie_id";
            $result2 = $conn->query($sql2);
            }
        ?>
    </footer>
  </body>
</html>
